package br.com.senac.cachoeira.view;

import android.content.ClipData;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.opengl.EGLExt;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;

import br.com.senac.cachoeira.R;
import br.com.senac.cachoeira.model.Cachoeira;

public class MainActivity extends AppCompatActivity {

    public static final  int  REQUEST_NOVO = 1 ;

    private static Bitmap imagem   ;

    public static Bitmap getImagem(){
        return imagem;
    }


    public static final String CACHOEIRA = "cachoeira" ;
    public static final String  IMAGEM =  "imagem" ;


    private ListView listView  ;
    private List<Cachoeira> lista  = new ArrayList<>() ;
    private ArrayAdapter<Cachoeira> adapter  ;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        /* inicializando cachoeiras padrao ..... */
        Cachoeira rioDoMeio = new Cachoeira();
        rioDoMeio.setId(1);
        rioDoMeio.setNome(getResources().getString(R.string.Rio_do_Meio));
        rioDoMeio.setInformocoes(getResources().getString(R.string.Rio_do_Meio_info));
        rioDoMeio.setClassificacao(Float.parseFloat(getResources().getString(R.string.Rio_do_Meio_classificacao)));
        rioDoMeio.setImagem(BitmapFactory.decodeResource(getResources() , R.drawable.rio_do_meio));
        rioDoMeio.setEmail("riodomeio@cachoeira.com.br.es");
        rioDoMeio.setEndereco("cidade Santa Leopodina - ES, 29142-602");
        rioDoMeio.setSite("http://www.leopoldina.com.br.es");
        rioDoMeio.setTelefone("(27) 988102901");




        Cachoeira matilde = new Cachoeira();
        matilde.setId(2);
        matilde.setNome(getResources().getString(R.string.Matilde));
        matilde.setInformocoes(getResources().getString(R.string.Matilde_info));
        matilde.setClassificacao(Float.parseFloat(getResources().getString(R.string.Matilde_classificacao)));
        matilde.setImagem(BitmapFactory.decodeResource(getResources(), R.drawable.matilde));
        matilde.setEmail("matilde@cachoeira.com.br.es");
        matilde.setEndereco("cidade Aldredo chaves - ES, 29123-157");
        matilde.setSite("http://www.cachoeirasemmatilde.com.br.es");
        matilde.setTelefone("(27) 998513650");


        Cachoeira fumaca = new Cachoeira();
        fumaca.setId(3);
        fumaca.setNome(getResources().getString(R.string.Cachoeira_da_Fumaca));
        fumaca.setInformocoes(getResources().getString(R.string.Cachoeira_da_Fumaca_info));
        fumaca.setClassificacao(Float.parseFloat(getResources().getString(R.string.Cachoeira_da_Fumaca_classificacao)));
        fumaca.setImagem(BitmapFactory.decodeResource(getResources() , R.drawable.cachoeira_da_fumaca));
        fumaca.setEmail("fumaca@cachoeira.com.br");
        fumaca.setEndereco("cidade Alegre - ES, 27111-2015");
        fumaca.setSite("http://www.cachoeiradealegra.com.br.es");
        fumaca.setTelefone("(27) 999922126");

        lista.add(rioDoMeio);
        lista.add(matilde);
        lista.add(fumaca);


        /* pegar elementos da view */
        listView = findViewById(R.id.ListaCachoeiras) ;

        int layout = android.R.layout.simple_list_item_1;

        adapter = new ArrayAdapter<Cachoeira>(this, layout , lista) ;

        listView.setAdapter(adapter);


        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapter, View contexto, int posicao, long indice) {

                Cachoeira cachoeira = (Cachoeira)adapter.getItemAtPosition(posicao) ;

                Intent intent = new Intent(MainActivity.this , DetalheActivity.class) ;
                intent.putExtra(CACHOEIRA , cachoeira);

                imagem = cachoeira.getImagem() ;


                /* compactar a imagem para ser passada na intencao
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                cachoeira.getImagem().compress(Bitmap.CompressFormat.PNG , 100 , stream) ;
                byte[] bytes = stream.toByteArray() ;


                intent.putExtra(IMAGEM , bytes) ;

                */


                startActivity(intent);


            }
        });




    }






    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu , menu);

        return true ;
    }

    public void novo(MenuItem item){

        Intent intent = new Intent(this , NovoActivity.class);
        startActivityForResult(intent , REQUEST_NOVO);


    }
    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {

        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo ;
        cachoeiraSelecionada = (Cachoeira) adapter.getItem(info.position);


        MenuItem itemMenuSite =  menu.add("Visitar site");
        Intent intentSite = new Intent(Intent.ACTION_DEFAULT) ;
        String site = "www.google.com";

        //www.google.com.br

        if(!site.startsWith("http://")){
            site = "http://" + site ;
        }

        intentSite.setData(Uri.parse(site)) ;
        itemMenuSite.setIntent(intentSite) ;







        menu.add("Enviar E-mail") ;
        menu.add("Ligar") ;

        menu.add("Como Chegar") ;
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if(requestCode == REQUEST_NOVO){

            switch (resultCode){
                case RESULT_OK :
                    Cachoeira cachoeira = (Cachoeira) data.getSerializableExtra(MainActivity.CACHOEIRA);
                    cachoeira.setImagem(NovoActivity.getImagem());
                    lista.add(cachoeira);
                    adapter.notifyDataSetChanged();
                    break;
                case RESULT_CANCELED :
                        Toast.makeText(this, "Cancelou" , Toast.LENGTH_LONG).show();
                        break;

            }

        }



    }

    public void sobre (MenuItem item){
        Intent intent = new Intent(this , SobreActivity.class) ;
        startActivity(intent);


    }





















}
